import requests
from PIL import Image
from io import BytesIO
import traceback
import os
import time
print (os.path.dirname(__file__))
urlPresidentInit = "https://trep.oep.org.bo/resul/imgActa/"
# urlPresidentInit = "https://computo.oep.org.bo/resul/imgActa/"
urlPresidentEnd = "1.jpg"
idDB = input("Ingrese un numero de BD a leer 1,2,3,4 o 5: ")
if idDB == "1" or idDB == "2" or idDB == "3" or idDB == "4" or idDB == "5":
    db = open("database"+idDB+".txt", "r")
    for dep in db:
        fields = dep.split(";")
        print (fields)
        try:
            os.mkdir(os.path.join(".","Download",fields[0]))
        except:
            traceback.print_exc()
        f = open(os.path.join(".","Download",fields[0], fields[0]+".txt"), "a")
        # f.write("Start Run "+str(int(round(time.time() * 1000)))+"\n")
        mesaStart = int(fields[1])
        mesaEnd = int(fields[2])
        for i in range(mesaStart,mesaEnd+1) :
            url = urlPresidentInit+str(i)+urlPresidentEnd
            headers = {
                'Accept': "*/*",
                'Cache-Control': "no-cache",
                'Accept-Encoding': "gzip, deflate",
                'Cookie': "__cfduid=dd9aa6812787cec11d38aedea98ca3a881571688550",
                'Connection': "keep-alive",
                'cache-control': "no-cache"
                }
            response = requests.request("GET", url, headers=headers)
            print (response)
            try:
                down = Image.open(BytesIO(response.content))
                down.save(os.path.join(".","Download",fields[0], str(i)+urlPresidentEnd))
            except:
                f.write(str(i)+";")
                traceback.print_exc()
        # f.write("EndRun\n")
        f.close()
print ("END RUN")
